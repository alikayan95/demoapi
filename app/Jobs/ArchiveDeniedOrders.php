<?php

namespace App\Jobs;

use App\Models\ArchivedOrder;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ArchiveDeniedOrders implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        DB::beginTransaction();
        try {

            $create_at = Carbon::now();
            $deniedOrder = Order::where('status', -1)->get()->map(function (Order $order, int $key) use ($create_at) {
                return [
                    'user_id' => $order->user_id,
                    'status' => -1,
                    'products' => $order->products,
                    'created_at' => $create_at,
                ];
            });
            if ($deniedOrder) {
                ArchivedOrder::insert($deniedOrder->toArray());
                //todo ilişkiyi cascade olarak ayarla
//                Order::where('status', -1)->products->delete();
                Order::where('status', -1)->delete();
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            logger()->critical('Kayıtlar Arşivlenemedi.' . $e->getMessage(),);
        }

    }
}
