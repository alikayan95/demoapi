<?php

namespace App\Listeners;

use App\Events\OrderNotifyEvent;
use App\Notifications\OrderNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OrderNotifySender
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(OrderNotifyEvent $event): void
    {
        $event->order->notify(new OrderNotification($event->newOrder));
    }
}
