<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        $status = "";
        if($this->status == 0){
            $status = "Onay Bekliyor";
        }else if($this->status == 1){
            $status = "Onaylandı";
        }else{
            $status = "Reddedildi";
        }
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'products' => OrderProductResource::collection($this->products),
            'status' => $status,
        ];
    }
}
