<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\OrderNotifyEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::paginate(25);

        return response()->json([
            'message' => 'Siparişler başarıyla listelendi',
            'data' => [
                'orders' => OrderResource::collection($orders)
            ]
        ]);
    }

    public function changeOrderStatus(Request $request)
    {
        if ($request->status < -1 || $request->status > 1) {
            return response()->json([
                'message' => 'Geçersiz sipariş durumu',
            ]);
        }
        $order = Order::find($request->id);
        if ($order) {
            $order->status = $request->status;
            $order->save();
            event(new OrderNotifyEvent($order,false));
            return response()->json([
                'message' => 'Sipariş durumu başarı ile değiştirildi.',
                'data' => new OrderResource($order)
            ]);
        } else {
            return response()->json([
                'message' => 'Sipariş Bulunamadı.',
            ]);
        }
    }
}
