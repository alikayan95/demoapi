<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\OrderNotifyEvent;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ShoppingCardResource;
use App\Models\Order;
use App\Models\OrderProducts;
use App\Models\Product;
use App\Models\ShoppingCart;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\JsonResponse;

class ShoppingCartController extends Controller
{
    public function show(ShoppingCartController $shoppingCart)
    {
        $shoppingCart = ShoppingCart::where('user_id', auth()->user()->id)->paginate(25);
        return response()->json([
            'message' => 'Sepet başarıyla listelendi',
            'data' => [
                'products' => ShoppingCardResource::collection($shoppingCart)
            ]
        ]);
    }

    public function store(Request $request)
    {
        $product = Product::find($request->productId);
        if (!$product) {
            return response()->json([
                'message' => 'Ürün bulunamadı.',
            ]);
        }
        $user = auth()->user();
        $productHasACart = ShoppingCart::where('user_id', $user->id)->where('product_id', $request->productId)->count();
        if (!$productHasACart) {
            $cartProduct = ShoppingCart::create([
                'product_id' => $product->id,
                'user_id' => auth()->user()->id,
                'price' => $product->price,
                'quantity' => 1,
            ]);
        } else {
            $cartProduct = ShoppingCart::where('user_id', $user->id)->where('product_id', $request->productId)->first();
            $cartProduct->increseQuatity();
            $cartProduct->save();
        }
        return response()->json([
            'message' => 'Ürün başarı ile sepete eklendi.',
            'data' => [
                ShoppingCardResource::make($cartProduct),
            ]
        ]);
    }

    public function decreaseProduct(Request $request)
    {
        $productCart = ShoppingCart::where('user_id', auth()->user()->id)->where('product_id', $request->productId)->first();
        if (!$productCart)
            return response()->json([
                'message' => 'Ürün sepette bulunamadı.',
                'data' => [
                ]
            ], 404);
        if ($productCart->quantity > 1) {
            $productCart->decreaseQuatity();
            $productCart->save();
            return response()->json([
                'message' => 'Ürün başarı ile azaltıldı.',
                'data' => [
                    'products' => $productCart
                ]
            ]);
        } else {
            $productCart->delete();
            return response()->json([
                'message' => 'Ürün başarı ile kaldırıldı.',
                'data' => [

                ]
            ]);
        }
    }

    public function increaseProduct(Request $request)
    {
        $productCart = ShoppingCart::where('user_id', auth()->user()->id)->where('product_id', $request->productId)->first();
        if ($productCart) {
            $productCart = ShoppingCart::where('user_id', auth()->user()->id)->where('product_id', $request->productId)->first();
            $productCart->increseQuatity();
            $productCart->save();
            return response()->json([
                'message' => 'Ürün başarı ile artırıldı.',
                'data' => [
                    'products' => $productCart
                ]
            ]);
        } else {
            $product = Product::find($request->productId);
            $productCart = ShoppingCart::create([
                'product_id' => $product->id,
                'user_id' => auth()->user()->id,
                'price' => $product->price,
                'quantity' => 1,
            ]);

        }
        return response()->json([
            'message' => 'Ürün başarı ile artırıldı.',
            'data' => [
                'products' => $productCart
            ]
        ]);

    }

    public function destroy(Request $request,ShoppingCartController $shoppingCart)
    {
        $productCart = ShoppingCart::where('user_id', auth()->user()->id)->where('product_id', $request->productId)->delete();
//        dd($productCart);
        return response()->json([
            'message' => $productCart ? 'Ürün başarı ile kaldırıldı.' : 'Ürün sepette bulunamadı.',
            'data' => [
            ]
        ]);
    }
    public function checkOut(Request $request)
    {
        $user = auth()->user();
        $shoppingCart = ShoppingCart::where('user_id', $user->id)->get();
        if (count($shoppingCart) == 0) {
            return response()->json([
                'message' => 'Sepette Ürün Bulunamadı.',
                'data' => []
            ]);
        }
        $order = null;
        DB::beginTransaction();
        try {
            $create_at = Carbon::now();
            $order = Order::create([
                'user_id' => $user->id,
            ]);
            $cartProducts = $shoppingCart->map(function (ShoppingCart $product, int $key) use($create_at,$order) {
                return [
                    'order_id' => $order->id,
                    'product_id' => $product->id,
                    'quantity' => $product->quantity,
                    'created_at' => $create_at,
                ];
            });
            OrderProducts::insert($cartProducts->toArray());
            ShoppingCart::where('user_id', $user->id)->delete();
            DB::commit();
            event(new OrderNotifyEvent($order,true));
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
            ]);
        }
        return response()->json([
            'message' => 'Sipariş Oluşturuldu.',
            'data' => [
                'order' => OrderResource::make($order),
            ]
        ]);
    }
}
