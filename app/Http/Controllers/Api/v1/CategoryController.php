<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return response()->json([
            'message' => 'Kategoriler başarıyla listelendi',
            'data' => [
                'categories' => $categories
            ]
        ]);
    }

    public function show($categoryId)
    {
        $category = Category::find($categoryId);
        if (!$category) {
            return response()->json([
                'message' => 'Kategori bulunamadı',
                'data' => []
            ], 404);
        }

        $products = Product::where('category_id', $categoryId)->paginate(25);
        return response()->json([
            'message' => 'Kategori başarıyla listelendi',
            'data' => [
                'category' => $category,
                'products' => $products
            ]
        ]);
    }
}
