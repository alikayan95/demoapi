<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderNotification extends Notification implements ShouldQueue
{
    use Queueable;

    protected $newOrder;

    /**
     * Create a new notification instance.
     */

    public function __construct(bool $newOrder)
    {
        $this->newOrder = $newOrder;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        $message = "";
        if ($this->newOrder) {
            $message = 'Siparişiniz Başarı ile Alındı.';
        } else {
            if ($notifiable->status == -1) {
                $message = 'Siparişiniz Reddedildi.';
            } elseif ($notifiable->status == 1) {
                $message = 'Siparişiniz Onaylandı.';
            } else {
                $message = 'Siparişiniz Onay Bekliyor.';
            }
        }
        return [
            'message' => $message,
            'data' => [
                $notifiable->load("user")->toArray(),
            ]
        ];
    }
}
