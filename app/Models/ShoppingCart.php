<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use App\Models\User;
use App\Models\Product;

class ShoppingCart extends Model
{
    use HasFactory;

    protected $table = "shopping_carts";

    protected $fillable = [
        'product_id',
        'price',
        'user_id',
        'quantity'
    ];
    public function product() : BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class,"user_id");
    }

    public function increseQuatity()
    {
        $this->quantity = $this->quantity + 1;
        $this->price = $this->quantity * $this->product()->first()->price;
    }

    public function decreaseQuatity()
    {
        $this->quantity = $this->quantity -1;
        $this->price = $this->quantity * $this->product()->first()->price;
    }

}
