<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\v1\AuthController;
use App\Http\Controllers\Api\v1\ProductController;
use App\Http\Controllers\Api\v1\CategoryController;
use App\Http\Controllers\Api\v1\ShoppingCartController;
use App\Http\Controllers\Api\v1\OrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
//
//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
//
//Route::post('check-user', [AuthController::class, 'checkUser']);
//Route::post('login', [AuthController::class, 'authenticate']);
//Route::post('register', [AuthController::class, 'register']);
//
//Route::middleware('auth:sanctum')->group(function () {
//    Route::post('update-profile', [AuthController::class, 'updateProfile']);
//
//    Route::get('/categories', [CategoryController::class, 'index']);
//    Route::get('/categories/{category_id}', [CategoryController::class, 'show']);
//
//    Route::get('/products', [ProductController::class, 'index']);
//    Route::get('/products/{product_id}', [ProductController::class, 'show']);
//
//    Route::get('/shoppingCart', [ShoppingCartController::class, 'show']);
//    Route::get('/addNewShoppingCartProduct', [ShoppingCartController::class, 'store']);
//    Route::get('/increaseProductToShoppingCart', [ShoppingCartController::class, 'increaseProduct']);
//    Route::get('/decreaseProductToShoppingCart', [ShoppingCartController::class, 'decreaseProduct']);
//    Route::get('/removeProductToShoppingCart', [ShoppingCartController::class, 'destroy']);
//    Route::get('/checkOutShoppingCart', [ShoppingCartController::class, 'checkOut']);
//
//    Route::get('/orders', [OrderController::class, 'index']);
//    Route::get('/changeOrderStatus', [OrderController::class, 'changeOrderStatus']);
//
//    Route::get('/getUserNotification', function (Request $request) {
//        return auth()->user()->orders->load("notifications");
//    });
//
//});
//
//Route::get('/getCategoriesWithProducts', function (Request $request) {
//    return \App\Models\Category::with("products")->get();
//});
//Route::get('/getProductWithCategory', function (Request $request) {
//    return \App\Models\Product::with("category")->find(1);
//});
//Route::get('/getUserShoppingCart', function (Request $request) {
//    return \App\Models\User::find($request->userId)->cartProducts;
//});
//Route::get('/getUserOrders', function (Request $request) {
//    return \App\Models\User::with("orders.products")->find($request->userId);
//});

